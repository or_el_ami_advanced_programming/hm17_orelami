#pragma once

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cstdio>
#include <iostream>
#include <string.h>
#include <cstdlib>
#include <md5.h>
#include <hex.h>
#include "osrng.h"
#include "modes.h"

class CryptoDevice
{

public:
	std::string encryptAES(std::string);
	std::string decryptAES(std::string);
	std::string encryptMD5(std::string);

private:
	CryptoPP::byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];

};
